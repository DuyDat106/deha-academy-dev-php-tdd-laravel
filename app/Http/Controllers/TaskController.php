<?php

namespace App\Http\Controllers;

use App\Http\Requests\EditTaskRequest;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Laravel\Ui\Presets\React;
use PHPUnit\Framework\Attributes\Test;

class TaskController extends Controller
{    
   protected $task;
   public function __construct(Task $task) {
    $this->task = $task;
   } 
   public function update(EditTaskRequest $request, $id)
   {
        $this->task->find($id)->update($request->all());
        return response()->json([], Response::HTTP_OK);
   }
   public function edit($id)
   {
        $task = $this->task->find($id);
        return view('tasks.edit', compact('task'));
   }
}
