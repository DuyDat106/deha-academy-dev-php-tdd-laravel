@extends('layouts.app')
@section('content')
    <div class="container">
        <form action="{{route('tasks.update', $task->id)}}" method="POST">
            @csrf
            @method('PUT')
            <input name="name" type="text" placeholder="name...">
            <input name="content" type="text" placeholder="content...">
            <button class="btn btn-success">submit</button>
        </form>
    </div>
@endsection