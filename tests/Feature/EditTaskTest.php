<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class EditTaskTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_edit_task()
    {
        $this->actingAs(User::factory()->create());
        $taskUpdate = Task::factory()->make()->toArray();
        $task = Task::factory()->create();
        $response = $this->put($this->getEditTaskRoute($task->id), $taskUpdate);
        $this->assertDatabaseHas('tasks', $taskUpdate);
        $response->assertStatus(Response::HTTP_OK);
    }
    /** @test */
    public function unauthenticated_user_can_not_edit_task()
    {
        $taskUpdate = Task::factory()->make()->toArray();
        $task = Task::factory()->create();
        $response = $this->put($this->getEditTaskRoute($task->id), $taskUpdate);
        $response->assertRedirect('/login');
    }
    /** @test */
    public function authenticated_user_can_not_edit_task_if_nama_is_null()
    {
        $this->actingAs(User::factory()->create());
        $taskUpdate = Task::factory()->make(['name'=>null])->toArray();
        $task = Task::factory()->create();
        $response = $this->put($this->getEditTaskRoute($task->id), $taskUpdate);
        $response->assertSessionHasErrors(['name']);
    }
    /** @test */
    public function authenticated_user_can_view_edit_task()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $response = $this->get($this->getEditTaskViewRoute($task->id));
        $response->assertViewIs('tasks.edit');
    }
    /** @test */
    public function authenticated_user_can_see_name_text_field_if_validate_error()
    {
        $this->actingAs(User::factory()->create());
        $taskUpdate = Task::factory()->make(['name'=>null])->toArray();
        $task = Task::factory()->create();
        $response = $this->from($this->getEditTaskViewRoute($task->id))->put($this->getEditTaskRoute($task->id), $taskUpdate);
        $response->assertRedirect($this->getEditTaskViewRoute($task->id));
    }
    /** @test */
    public function unauthenticated_user_can_not_view_edit_task_form()
    {
        $taskUpdate = Task::factory()->make()->toArray();
        $task = Task::factory()->create();
        $response = $this->from($this->getEditTaskViewRoute($task->id))->put($this->getEditTaskRoute($task->id), $taskUpdate);
        $response->assertRedirect('/login');
    }

    
    

    public function getEditTaskRoute($id)
    {
        return route('tasks.update', $id);
    }
    public function getEditTaskViewRoute($id)
    {
        return route('tasks.edit', $id);
    }
}
